#!/bin/bash
#
# ~/.bash_profile
#

# XDG dir definitions
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export GOPATH="$XDG_DATA_HOME/go"
export GOBIN="$GOPATH/bin"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export DVDCSS_CACHE="$XDG_DATA_HOME/dvdcss"
export WINEPREFIX="$XDG_DATA_HOME/wine"
export MEDNAFEN_HOME="$XDG_CONFIG_HOME/mednafen"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"
export RENPY_PATH_TO_SAVES="$XDG_DATA_HOME"
export FCEUX_HOME="$XDG_CONFIG_HOME/fceux"
export SQLITE_HISTORY="$XDG_CACHE_HOME/sqlite_history"

# Preferred programs
export EDITOR=nvim
export VISUAL=nvim
export PAGER=less
export BROWSER=lynx

# Disable LSP on Neovim on uwuland
if [ "$(hostname)" = "uwuland.lisais.gay" ]; then
	export NO_LSP=1
fi

# Less settings
export LESSHISTFILE=/dev/null
export LESS='--mouse -F -g -i -r -x4 -M -S'
export LESSCHARSET='utf-8'

# Prepends an item to PATH if the directory exists
function addLocalPath () {
	[ -d "$1" ] && PATH="$1:$PATH"
}

# Appends an item to PATH if the directory exists
function addSystemPath () {
	[ -d "$1" ] && PATH="$PATH:$1"
}

# Add local paths that exist
addLocalPath "$XDG_CONFIG_HOME/emacs/bin"
addLocalPath "$CARGO_HOME/bin"
addLocalPath "$GOPATH/bin"
addLocalPath "$HOME/.local/bin"
addLocalPath "$HOME/bin"

unset appendPath
unset prependPath

# Source bashrc
[[ -f "$HOME/.bashrc" ]] && . "$HOME/.bashrc"

