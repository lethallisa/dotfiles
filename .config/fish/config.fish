#!/bin/fish

# XDG dir definitions
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export GOPATH="$XDG_DATA_HOME/go"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export DVDCSS_CACHE="$XDG_DATA_HOME/dvdcss"
export WINEPREFIX="$XDG_DATA_HOME/wine"
export MEDNAFEN_HOME="$XDG_CONFIG_HOME/mednafen"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"
export RENPY_PATH_TO_SAVES="$XDG_DATA_HOME"
export FCEUX_HOME="$XDG_CONFIG_HOME/fceux"

# Preferred programs
export EDITOR=nvim
export VISUAL=nvim
export PAGER=less
export BROWSER=lynx

# Less settings
export LESSHISTFILE=/dev/null
export LESS="--mouse -F -g -i -r -x4 -M -S"
export LESSCHARSET='utf-8'

# Set up local path
export PATH="$XDG_CONFIG_HOME/emacs/bin:$HOME/.local/bin:$GOPATH/bin:$CARGO_HOME/bin:$PATH"

if status is-interactive
	function condsource
		[ -f "$1" ] && source "$1"
	end

	# Load Starship if interactive
	starship init fish | source

	# Load Zoxide if interactive
	zoxide init --cmd cd fish | source

	# GCC colors
	export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

	# Conditionally source extensions
	condsource '/usr/share/doc/pkgfile/command-not-found.fish'

	# ls aliases
	alias ls='ls --color=auto '
	alias l='ls -CF '
	alias la='ls -ACF '
	alias ll='ls -lAF '

	# Eza aliases
	alias ell='eza -lhF --icons'
	alias el='eza -F --icons'
	alias ela='eza -aF --icons'
	alias ella='eza -lhaF --icons'

	# Aliases to conform to XDG directories
	alias wget='wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'
	alias dosbox='doxbox -conf "$XDG_CONFIG_HOME/dosbox/dosbox.conf"'
	alias dhex="dhex -f $XDG_CONFIG_HOME/dhexrc"
	alias irssi='irssi --config="$XDG_CONFIG_HOME/irssi/config" --home="$XDG_DATA_HOME/irssi"'

	# Alias away more
	alias more=less

	# Alias for working with dotfiles repo.
	alias config='/usr/bin/git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
end

