#!/bin/bash
#
# ~/.bash_aliases
#

# Colors for all the different greps
alias grep='grep --color=auto '
alias fgrep='grep --color=auto '
alias egrep='grep --color=auto '

# Colorization for directory listing.
alias ls='ls --color=auto '
alias dir='dir --color=auto '
alias vdir='vdir --color=auto '

# ls aliases for faster navigation.
alias l='ls -CF '
alias la='ls -ACF '
alias ll='ls -lAF '

# Eza aliases to remember settings.
alias ell='eza -lhF --icons '
alias el='eza -F --icons '
alias ela='eza -aF --icons '
alias ella='eza -lhaF --icons '

# Aliases to make programs respect XDG directories.
alias wget='wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'
alias dosbox='dosbox -conf "$XDG_CONFIG_HOME/dosbox/dosbox.conf"'
alias dhex="dhex -f $XDG_CONFIG_HOME/dhexrc"
alias irssi='irssi --config="$XDG_CONFIG_HOME/irssi/config" --home="$XDG_DATA_HOME/irssi"'

# Alias away more.
alias more=less

# Use alias config to manage dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
