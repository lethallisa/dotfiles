#!/bin/bash
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# History settings
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=2000
HISTFILESIZE=3000
HISTFILE="$XDG_STATE_HOME/bash/history"

# Update window size
shopt -s checkwinsize

# Correct minor spelling mistakes in cd commands
shopt -s cdable_vars
shopt -s cdspell
shopt -s dirspell

# Show stopped jobs when exiting
shopt -s checkjobs

# Use tab cycling for completion
bind TAB:menu-complete
bind '"\e[Z":menu-complete-backward'

bind "set show-all-if-ambiguous on"
bind "set menu-complete-display-prefix on"

# has() checks for a list of programs' existence on $PATH and if it finds one
# that doesn't exist then it exits with code 1.
function has() {
	for program in "$@"; do
		if ! command -v "${program}" &>/dev/null; then
			exit 1
		fi
	done
}

# Function to conditionally source a file if it exists
function condsource() {
	[[ -f "$1" ]] && source "$1"
}

# Set prompt strings
function incErrCodePrompt {
	if [[ $? -ne 0 ]]; then
		PS1='\[\033[01;35;49m\][\u@\h\[\033[00m\]: \[\033[01;31m\]($?) \[\033[01;34m\]\w\[\033[01;35m\]]\$\[\033[00m\] '
	else
		PS1='\[\033[01;35;49m\][\u@\h\[\033[00m\]: \[\033[01;34m\]\w\[\033[01;35m\]]\$\[\033[00m\] '
	fi
}
PROMPT_COMMAND=incErrCodePrompt

# Optionally start Starship if it is installed and no_starship=false (default)
if has starship; then
	no_starship=${no_starship:-false}
	[[ $no_starship = false ]] && eval "$(starship init bash)"
fi

# Optionally start Zoxide if it is installed and no_zoxide=false (default)
if has zoxide; then
	no_zoxide=${no_zoxide:-false}
	[[ $no_zoxide = false ]] && eval "$(zoxide init --cmd cd bash)"
fi

# Export colors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Certain distros like Alpine don't include `dircolors`
if has dircolors; then
	source <(dircolors -b)
fi

# Include aliases file
condsource "$HOME/.bash_aliases"

# command-not-found handling for Arch Linux through pkgfile. This is specific to Arch Linux.
condsource '/usr/share/doc/pkgfile/command-not-found.bash'

# Completion for "config" git frontend. This is for handling dotfiles through a bare git repository.
condsource '/usr/share/bash-completion/completions/git' && __git_complete config __git_main

# Remove condsource and has functions so we don't clutter environment
unset condsource
unset has
