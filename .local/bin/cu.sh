#!/usr/bin/env sh

# Gale Misery Sodomy Unix Faraday's better checkupdates formatting and AUR
# integration for Arch Linux.
#
# Dependencies: pacman-contrib, yay
#
# If you use paru instead of yay you can simply replace instances of the word
# yay below.

for program in checkupdates yay; do
	if ! command -v "${program}" > /dev/null; then
		printf 'Cannot continue: missing "%s"!' "${program}"
		exit 1
	fi
done

repo_updates="$(checkupdates)"
aur_updates="$(yay -Qu)"

if [ -n "$repo_updates" ]; then
	printf 'Repo update count: %s\n%s\n' "$(echo "$repo_updates" | wc -l)" "$repo_updates"
fi

if [ -n "$aur_updates" ]; then
	printf 'AUR update count: %s\n%s\n' "$(echo "$aur_updates" | wc -l)" "$aur_updates"
fi
