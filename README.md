# Gale Faraday/Lethal Lisa's Dotfiles

This repo represents my current configuration for i3wm, and the applications
that I use. Being the present state of my configuration it's in a constant state
of flux and changes are frequent as I play with settings and find things that
are more or less to my liking. This configuration aims to use XDG directories
and modern specifications as much as is possible.

![screenshot](.screenie.png)

## Color Palette

For Qt, GTK2/3 and Wine applications, kokoscript's Loopy desktop theme is used,
however for i3 and Xresources things a similar custom theme is used. This is
similar to -- and indeed copied from -- the theme used on my
[website](https://lethallisa.neocities.org). The colors for this theme are best
viewed in the file `$XDG_CONFIG_HOME/i3/colors.conf` in the i3 config file.

## Software Used

This set of dotfiles includes configuration for:

* i3wm (window manager)
* Openbox (alternative window manager)
* `i3status` & `i3bar` (status bar)
* Xob (for volume level visualizer (requires `dash` and `python-pulsectl`))
* Kitty (terminal emulator)
* Alacritty (terminal emulator)
* Tmux (terminal multiplexer)
* Xosview2 (resource viewer)
* Dunst (notifications)
* Rofi (launcher)
* Bash (primary interactive shell)
* Fish (alternative shell)
* Nano (fallback text editor)
* SXIV (image viewer)
* Bat (pager with syntax highlighting)
* MPV (media player)
* Picom (compositor)
* Pipewire (sound server, configurations are provided for noise cancelling
  microphones)
* Newsboat (RSS/Atom reader)
* Xinit
* Wayland (custom initialization scripts for your own compositor)
* Qt5/6
* Starship (optional, can be overridden by setting `no_starship=true` when
  starting Bash. Default can also be overridden in `~/.bashrc`)
* Ranger (TUI file manager)

## Install

Clone the repository into a bare git repo at `~/.dotfiles`.

```sh
git clone --bare $mirror "$HOME/.dotfiles"
alias config='/usr/bin/git --git-dir="$HOME/.dotfiles" --work-tree="$HOME"'
config checkout
config config --local status.showUntrackedFiles no
```

Then re-login for settings to take effect. You may then use the `config` alias
to manage your local dotfiles, make edits as if you were using git, or `config
pull` down new changes from upstream.

---

## Shells

You can use Fish or Bash as your login shell. For Bash the order of execution is
as follows `~/.bash_profile`->`~/.bashrc`->`~/.bash_aliases`, and for Fish
`~/.config/fish/config.fish` is executed.

The files `~/.bash_profile` & `~/.config/fish/config.fish` set several default
programs. It sets Neovim (`nvim`) as the default values for `EDITOR` AND
`VISUAL` environment variables, `less` as `PAGER`, and `lynx` as `BROWSER`.

If the paths `~/.local/bin`, and/or `~/bin` exist, they will be prepended to the
`PATH` in that order.

If the file `~/.bash_aliases` exists it will be sourced by `~/.bashrc`. A
default `~/.bash_aliases` exists, it includes some Debian/Ubuntu-esque aliases
for `ls`, `grep`, `dir` and `vdir`. It aliases away `more` into `less`, and adds
some default settings for `nano`, and a `config` command for managing these
dotfiles with Git. Additionally there are aliases made to make using `eza` a
little more like using the `ls` bindings in this config. In the cases of Fish
all the aliases are handled in `~/.config/fish/config.fish`.

The Bash and Fish configs requires Starship, and Zoxide, but you can set the
variables `no_starship=true` and `no_zoxide=true` before `~/.bashrc` gets
executed. The Fish config does not yet have a way to disable these.

### Local Binaries

Several scripts are available under `~/.local/bin`.

* `cu.sh`: Check updates shell script for Arch Linux.
* `pulse-volume-watcher.py`: Python script for watching the current default
  pulseaudio sink's volume and piping it into Xob.
* `startw`: Startup script for a Wayland compositor.
* `startxob`: Script that tells i3wm to start Xob.

---

## Tmux

The `tmux` configuration uses [tpm](https://github.com/tmux-plugins/tpm), the
Tmux Package Manager. To provide a mode indicator, yanking, and Vim integration.
To install it run:

```sh
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

and then in `tmux` run `<prefix> I`, by default this is the default Tmux binding
for the prefix of `C-b`, so `C-b I`. You can use `<prefix> U` to upgrade TPM plugins.

---

## i3wm

### Required Programs

The i3 configuration requires the following programs:

* Dash (used to run background scripts)
* `i3lock` (screenlocker)
* `xss-lock` (screenlocker)
* Feh (wallpapers)
* Dex (XDG autostart)
* Dunst (notifications)
* `nm-applet` (easier network management)
* `pactl` (used internally to control volume level using volume keys)
* Pavucontrol (mixer started with `Mod4+p`)
* `i3bar` (status bar, what it says on the tin)
* Rofi (application launcher menu)
* Xrandr (used by `startx` to configure monitors, you will have to adjust these
  probably)
* `xorg-xinit`

### Workspaces

Several workspaces are configured by default:

* `1:term` is for system control and monitoring, basically this is intended as
  your desktop.
* `2:txt` for managing texts, PDF readers, text editors, and word processors.
* `3:browse` for browsers WWW and otherwise.
* `4:msg` for messaging and file sharing.

A few programs are bound to start up on specific workspaces.

### Launch Menu

The launch menu (by default accessed with `Mod4+m`) starts these programs:

* Firefox on `b`
* Thunar on `n`
* Audacious on `a`
* Emacs on `m`

---

## Openbox

Openbox is provided as an alternative window manager in case you don't like
tilers. It uses most of the same programs as the i3wm configuration, and can be
installed in parallel to i3wm. In order to start Openbox you can use the
command `startx ~/.xinitrc openbox` when starting the graphical environment.
The Openbox configuration is much younger compared to the i3wm configuration,
and I don't personally daily drive it, so it may or may not be as usable as the
i3wm configuration.

---

## Footnotes

### Rationale Behind using Dash

Dash, the Debian Almquist Shell, is used to handle a lot of startup scripts and
run pipes in the background. Xob is handled using it with the command `dash -c
"pulse-volume-watcher.py | xob"`, and handles screen locking and notification
locking. This is done because Dash tends to be faster than Bash. As for if this
is better in practice? I don't really care lol.

### Bugs

Currently there is an issue with
[`python-pulsectl`](https://aur.archlinux.org/packages/python-pulsectl) as
packaged on the AUR, and so for the time being, Xob is not enabled, but you may
still manually attempt to start it using `startxob`.

## Licensing

Many pieces of software here are under their various licenses, these dotfiles
themselves are under the Unlicense, and are to be considered public domain.

<!--
  vim: et sw=2 ts=2 tw=80
-->
